#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#define PISUAK_HOTEL ".\\data\\pisuakHotel.txt"
#define PISUAK_MUPARK ".\\data\\pisuakMUpark.txt"
#define NODOAK_HOTEL ".\\data\\nodoakHotel.txt"
#define NODOAK_MUPARK ".\\data\\MUpark_nodoak.txt"
#define INFINITO 99999
#define MAX 120

int irakurriPisuak(MAPA* mapa, char fitxIzena[], int h, int b, int mapa_id);
NODO* irakurriNodoa(NODO* ptrelem, char fitxIzena[], MAPA* mapa, NODO* ptrlehena);
NODO* sartuBukaeran(NODO* lehena, NODO* berria);
void dijkstra(MAPA* mapa, int G[MAX][MAX], int nodoKop, int hasieranodoa, int amaieranodoa, int mapa_id);
#endif
