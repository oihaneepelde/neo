#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <SDL.h>
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"
#include "argazkiakEtaSoinuak.h"
#include "menua.h"
#include "dijkstra.h"

void PantailaKargatu(char izena[])
{
	int id;
	id = irudiaKargatu(izena);
	pantailaGarbitu();
	irudiakMarraztu();
	pantailaBerriztu();
}
void PantailaKargatuKoord(char izena[],int x,int y)
{
    int id;
    id = irudiaKargatu(izena);
    pantailaGarbitu();
    irudiakMarraztu();
    irudiaMugitu(id, x, y);
    pantailaBerriztu();
}
void banderaKargatu(int x, int y)
{
	int id;
	id = irudiaKargatu(BANDERA);
	//pantailaGarbitu();
	irudiaMugitu(id, x, y);
	irudiakMarraztu();
	//pantailaBerriztu();
}
int txakurraKargatu(int x, int y)
{
	int id;
	id = irudiaKargatu(TXAKURRA);
	irudiaMugitu(id, x, y);
	irudiakMarraztu();
	return id;
}

int nodoakAukeratu(MAPA* mapak, int nodokop) //HASIERA ETA AMAIERA PUNTUAK LORTU
{
	int nodoak = 0, ebentu = 0, i = 0, nodoa = 0,j;
	POSIZIOA pos;
    NODO* aux = NULL;
    MAPA* mapa_aux=NULL;
    mapa_aux = mapak;
    
	do
	{
		pos = saguarenPosizioa();
		ebentu = ebentuaJasoGertatuBada();
		
        for( i = 0 ; i < nodokop && nodoak!=1; i++)
         {
            if (i == 0)aux = mapa_aux->ptrNodoak;
            else
            {
                aux = mapa_aux->ptrNodoak;
                for (j = 0; j <= i - 1 && aux->ptrHurrengoa!=NULL; j++)
                {
                    aux = aux->ptrHurrengoa;
                }
            }
                
                if (pos.x > aux->x1 && pos.x < aux->x2 && pos.y > aux->y1 && pos.y < aux->y2)//LORATEGIA
                {
                    if (ebentu == SAGU_BOTOIA_EZKERRA)
                    {
                        nodoa = i + 1;
                        arkatzKoloreaEzarri(255, 87, 51);
                        zirkuluaMarraztu(pos.x, pos.y, 20);
                        pantailaBerriztu();
                        nodoak = 1;
                        Sleep(900);
                    }
                }
               
        }
		
	} while (nodoak != 1);
	return nodoa;
}
void sarreraMenua()
{
	MAPA* mapak = NULL;
	POSIZIOA pos;
    NODO* ptrelem = NULL;
	int ebentu = 0;
	int irten = 0, ebentuGertatua = 0;
	int kargatu = 0, azalpena = 0;
	int azalpenSakona = 0, kontaktuak = 0;
	int ondo = 0, mapa = 0;
    char* mapa_iz;
	PantailaKargatu(HASIERAKO_PANTAILA);
	do
	{
		pos = saguarenPosizioa();
		ebentu = ebentuaJasoGertatuBada();
        pos = pasahitza_eskatu(pos,&ebentu,&ondo);
		if (ebentu == TECLA_RETURN) //SARTU
		{
				if (ondo == 1)
				{
					PantailaKargatu(MENUA);
					do
					{
						pos = saguarenPosizioa();
						ebentu = ebentuaJasoGertatuBada();
						if (pos.x > 296 && pos.x < 679 && pos.y > 215 && pos.y < 305) //MAPA AUKERATU PANTAILA KOORDENATUAK
						{
							if (ebentu == SAGU_BOTOIA_EZKERRA)
							{
								PantailaKargatu(MAPA_AUKERATU);                                
								mapa = 0;
                                ebentu = 0;
								do
								{
									pos = saguarenPosizioa();									
									ebentu = ebentuaJasoGertatuBada();
                                    if (ebentu == SAGU_BOTOIA_EZKERRA)//MAPEN ZERRENDA SORTU
                                    {                                        
                                        mapa_iz = izenak(pos);
                                        free(mapak);
                                        mapak = sortumapa(mapa_iz);
                                        
                                    }
                                    pos = mapa_aukeratu(pos,ebentu,mapak,&mapa);
									if (pos.x > 900 && pos.x < 1000 && pos.y > 40 && pos.y < 90)
									{
										if (ebentu == SAGU_BOTOIA_EZKERRA)
										{
											mapa = 1;
                                            PantailaKargatu(MENUA);
										}
									}
								} while (mapa != 1);
							}
						}
						if (pos.x > 297 && pos.x < 660 && pos.y > 365 && pos.y < 445)
						{
							if (ebentu == SAGU_BOTOIA_EZKERRA)
							{
								PantailaKargatu(AZALPEN_OROKORRA); //AZALPEN OROKORRERA JOATEKO
								azalpena = 0;
								do
								{
									pos = saguarenPosizioa();
									ebentu = ebentuaJasoGertatuBada();
									if (pos.x > 638 && pos.x < 957 && pos.y > 445 && pos.y < 688) //POSIZIO HAU AZALPEN SAKONEKO KOORDENADAK
									{
										if (ebentu == SAGU_BOTOIA_EZKERRA)
										{
											PantailaKargatu(AZALPEN_SAKONA);
											azalpenSakona = 0;
                                            pos = atzera(&azalpenSakona);
										}
									}
									if (pos.x > 900 && pos.x < 1000 && pos.y > 40 && pos.y < 90) //ATZERA KOORDENADAK AZALPEN OROKORRA PANTAILA
									{
										if (ebentu == SAGU_BOTOIA_EZKERRA)
										{
                                            PantailaKargatu(MENUA);
											azalpena = 1;
										}
									}
								} while (azalpena != 1);
							}
						}
						if (pos.x > 299 && pos.x < 671 && pos.y > 505 && pos.y < 585) //KONTAKTUAK KOORDENADAK MENUTIK
						{
							if (ebentu == SAGU_BOTOIA_EZKERRA)
							{
								PantailaKargatu(KONTAKTUAK);
								kontaktuak = 0;
                                pos = atzera(&kontaktuak);
							}
						}
						if (pos.x > 876 && pos.x < 1000 && pos.y > 595 && pos.y < 750)
						{
							if (ebentu == SAGU_BOTOIA_EZKERRA)
							{
								sgItxi();
							}
						}
					} while (irten != 1);
				}
				else
				{
					PantailaKargatuKoord(FRANGA_GAIZKI,500,500);
					Sleep(1500);
					sgItxi();
				}
			}
		if (pos.x > 876 && pos.x < 1000 && pos.y > 595 && pos.y < 750)
		{
			if (ebentu == SAGU_BOTOIA_EZKERRA)
			{
				sgItxi();
			}
		}
	} while (irten != 1);
}


MAPA* sortumapa(char izena[]) //MAPA IRAKURRI
{
    MAPA* mapak=NULL;
    NODO* ptrelem = NULL;;
    mapak = (MAPA*)malloc(sizeof(MAPA));
    if (mapak!=NULL)
    {
        mapak->ptrNodoak = NULL;
        mapak->ptrNodoak = irakurriNodoa(ptrelem, izena, mapak, mapak->ptrNodoak);
    }
        
    return mapak;
}
char* izenak(POSIZIOA pos)
{
    char* izena=NULL;
    izena = (char*)malloc(sizeof(char)*128);

    if (pos.x > 49 && pos.x < 351 && pos.y > 275 && pos.y < 500)
    {
        if (izena != 0)strcpy(izena, NODOAK_HOTEL);
    }
    else if (pos.x > 586 && pos.x < 885 && pos.y > 280 && pos.y < 510)
    {
        if (izena!=0)strcpy(izena, NODOAK_MUPARK);
    }
    return izena;
}

POSIZIOA atzera(int* elem)
{
    POSIZIOA pos;
    int ebentu;
    do
    {
        pos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (pos.x > 900 && pos.x < 1000 && pos.y > 40 && pos.y < 90)
        {
            if (ebentu == SAGU_BOTOIA_EZKERRA)
            {
                PantailaKargatu(MENUA);
                *elem = 1;
            }
        }
    } while (*elem != 1);

    return pos;
}
POSIZIOA mapa_aukeratu(POSIZIOA pos, int ebentu, MAPA* mapak, int*mapa) //AUKERATUTAKO MAPA KARGATU
{
    int h = 0, b = 0, id = 0;
    if (pos.x > 49 && pos.x < 351 && pos.y > 275 && pos.y < 500) //HOTELAREN KOORDENATUAK
    {
        if (ebentu == SAGU_BOTOIA_EZKERRA)
        {
            id = 1;
            PantailaKargatu(MAPA_HOTEL);

            h = nodoakAukeratu(mapak, mapak->nodokop);
            b = nodoakAukeratu(mapak, mapak->nodokop);
            irakurriPisuak(mapak, PISUAK_HOTEL, h, b,id);
            *mapa = 0;
            pos = atzera(mapa);
        }
    }
    if (pos.x > 586 && pos.x < 885 && pos.y > 280 && pos.y < 510) //MUPARK KOORDENATUAK
    {
        if (ebentu == SAGU_BOTOIA_EZKERRA)
        {
            PantailaKargatu(MAPA_MUPARK);
            id = 2;
            h = nodoakAukeratu(mapak, mapak->nodokop);
            b = nodoakAukeratu(mapak, mapak->nodokop);
            irakurriPisuak(mapak, PISUAK_MUPARK, h, b, id);
            *mapa = 0;
            pos = atzera(mapa);
        }
    }
    return pos;
}

POSIZIOA pasahitza_eskatu(POSIZIOA pos, int* ebentu, int*ondo)
{
    char pasahitza[128] = { "neo" };
    char sartutakoPasahitza[128] = {0};
    int strLuzera = 0;
    char str[128];
    int karaktereKop = 0;
    unsigned int i;
    if (pos.x > 262 && pos.x < 737 && pos.y > 360 && pos.y < 449)//PASAHITZA
    {
        if (*ebentu == SAGU_BOTOIA_EZKERRA)
        {
            PantailaKargatu(HASIERAKO_PANTAILA2);
            /*fgets(str, 128, stdin);
            sscanf(str, "%s", str);*/
            do
            {
                *ebentu = ebentuaJasoGertatuBada();
                if (*ebentu >= 'a' && *ebentu <= 'z' || *ebentu >= 'A' && *ebentu <= 'Z')
                {
                    str[0] = *ebentu;
                    str[1] = '\0';
                    if (karaktereKop == 0)
                    {
                        strcpy(sartutakoPasahitza, str);
                        karaktereKop++;
                    }
                    else
                    {
                        strcat(sartutakoPasahitza, str);
                    }
                    textuaGaitu_P();
                    //textuaIdatzi(522, 380, sartutakoPasahitza);
                    for (i = 0; i < strlen(sartutakoPasahitza); i++) textuaIdatzi(400 + (i * 30), 380, "* ");
                    pantailaBerriztu();
                }
                if (*ebentu == TECLA_SUPR)
                {
                    strLuzera = strlen(sartutakoPasahitza);
                    //ARKATZAREN KOROLEA FONDOKO BERDINERA JARRI ETA HORRELA EZABATU EFEKTU BAT GERTATUKO DA
                    if (strlen(sartutakoPasahitza) > 0)
                    {
                        for (i = 0; i < strlen(sartutakoPasahitza); i++) textuaTxuri(400 + (i * 30), 380, "* ");
                        //textuaTxuri(522, 380, sartutakoPasahitza);
                        pantailaBerriztu();

                        sartutakoPasahitza[strlen(sartutakoPasahitza) - 1] = '\0';

                        if (strlen(sartutakoPasahitza) > 0)
                        {
                            //textuaIdatzi(522, 380, sartutakoPasahitza);
                            for (i = 0; i < strlen(sartutakoPasahitza); i++) textuaIdatzi(400 + (i * 30), 380, "* ");
                            pantailaBerriztu();
                        }

                    }

                }
            } while (*ebentu != TECLA_RETURN);

            if (strcmp(pasahitza, sartutakoPasahitza) == 0)
            {
                *ondo = 1;
            }
        }
    }
    return pos;
}