#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <SDL.h>
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"
#include "argazkiakEtaSoinuak.h"
#include "menua.h"
#include "dijkstra.h"

int irakurriPisuak(MAPA* mapa, char fitxIzena[],int h, int b, int mapa_id) //PISUEN MATRIZEA IRAKURRI
{
	
	int G[MAX][MAX], i=0, j=0, aux,egoera=0;
	FILE* fitxategia = NULL;
	fitxategia = fopen(fitxIzena, "r");
    MAPA* mapa_aux = NULL;
    mapa_aux = mapa;

	for (int i = 0; i < mapa_aux->nodokop; i++)
	{
		for (int j = 0; j < mapa_aux->nodokop; j++)
		{
			egoera=fscanf(fitxategia, "%d", &aux);
			G[i][j] = aux;
		}
	}
	dijkstra(mapa, G, mapa_aux->nodokop, h-1, b-1,mapa_id);
	return 0;
}

void dijkstra(MAPA* mapa, int G[MAX][MAX], int nodoKop, int hasieranodoa, int amaieranodoa,int mapa_id)
{
    int kostea[MAX][MAX], distantzia[MAX], aurrekoa[MAX], pausuak[MAX] = {0}, bidea[MAX];
	int egonda[MAX], kont, mindistantzia, hurrengonodoa = 0, i, j, k = 0,amaiera=0;
	char str[128];
    MAPA* mapa_aux = NULL;
    mapa_aux = mapa;
	//NODO BATETIK BESTERAKO KOSTEA GORDE
	for (i = 0; i < nodoKop; i++)
		for (j = 0; j < nodoKop; j++)
			if (G[i][j] == 0)
				kostea[i][j] = INFINITO;
			else
				kostea[i][j] = G[i][j];

	//ALDAGAIAK HASIERATU
	for (i = 0; i < nodoKop; i++)
	{
		distantzia[i] = kostea[hasieranodoa][i];
		aurrekoa[i] = hasieranodoa;
		egonda[i] = 0;
	}

	distantzia[hasieranodoa] = 0;
	egonda[hasieranodoa] = 1;
	kont = 1;

	while (kont < nodoKop - 1)
	{
		mindistantzia = INFINITO;

		//DISTANTZIA TXIKIENEKO HURRENGO NODOA GORDE
		for (i = 0; i < nodoKop; i++)
			if (distantzia[i] < mindistantzia && !egonda[i])
			{
				mindistantzia = distantzia[i];
				hurrengonodoa = i;
			}

		//HURRENGONODOA AUKERATUTA BIDEA MOTZAGOA DEN EGIAZTATU			
		egonda[hurrengonodoa] = 1;
		for (i = 0; i < nodoKop; i++)
			if (!egonda[i])
				if (mindistantzia + kostea[hurrengonodoa][i] < distantzia[i])
				{
					distantzia[i] = mindistantzia + kostea[hurrengonodoa][i];
					aurrekoa[i] = hurrengonodoa;
				}
		kont++;
	}
	for (i = 0; i < nodoKop; i++)
	{
		if (i == amaieranodoa)
		{
			if (distantzia[i] == INFINITO) {
				printf("Ezin da puntu horretara iritsi.");
			}
			else {
				
				pausuak[k] = i + 1;
				j = i;
				do
				{
					j = aurrekoa[j];
					pausuak[k + 1] = j + 1;
					k++;
				} while (j != hasieranodoa);
			}
		}
	}
	//EMAITZAK PANTAILARATU, pausuak[] BIDEA GORDETZEN JOAN
	int id = 0;
    NODO* aux_h = NULL,*aux_a=NULL;
    aux_h = mapa_aux->ptrNodoak;
    for (i = 0; i < hasieranodoa; i++)
    {
        aux_h = aux_h->ptrHurrengoa;
    }
	id = txakurraKargatu(aux_h->x1, aux_h->y1);
    aux_a = mapa_aux->ptrNodoak;
    for (i = 0; i < amaieranodoa; i++)
    {
        aux_a = aux_a->ptrHurrengoa;
    }
	banderaKargatu(aux_a->x1 + 20, aux_a->y1 + 20);

	//DELAY BAT JARRI DUGU, PERSONAIA PAUSOZ PAUSOZ MUGITZEKO
	SDL_Delay(700);


	for (int j = 0; j <= k; j++) {//BIDEARI BUELTA EMAN, HASIERAKO PUNTUTIK
		bidea[j] = pausuak[k - j];
	}
	int x_h = 0,x=0,x_d=0;
	int y_h = 0,y=0,y_d=0;
    int h = 0;
    NODO* aux = NULL;
    aux = mapa_aux->ptrNodoak;
	if (distantzia[0] != INFINITO)
	{ // TXAKURRAREN MUGIMENDUA SIMULATU

		for (i = 0; i < k; i++)
		{
            aux = mapa_aux->ptrNodoak;
            for (h = 0; h < (bidea[i] - 1); h++)
            {
                aux = aux->ptrHurrengoa;
            }
			x_h = aux->x1;
			y_h = aux->y1;
            aux = mapa_aux->ptrNodoak;
            for (h = 0; h < (bidea[i+1]-1); h++)
            {
                if (aux->ptrHurrengoa!=NULL)aux = aux->ptrHurrengoa;
            }
            x_d = aux->x1;
            y_d = aux->y1;
            
            x = x_h;
            while (x != x_d)
            {
                irudiaMugitu(id, x, y_h);
                irudiakMarraztu();
                pantailaBerriztu();
                SDL_Delay(25);
                if (x_h < x_d) x++;
                if (x_h > x_d) x--;
            }
            
            y = y_h;
            while (y != y_d)
            {
                irudiaMugitu(id, x, y);
                irudiakMarraztu();
                pantailaBerriztu();
                SDL_Delay(25);
                if (y_h < y_d)y++;
                if (y_h > y_d)y--;
            }
                
            
		}
	}
	irudiaMugitu(id, aux_a->x1, aux_a->y1);
	irudiakMarraztu();
	for (i = 0; i <= k; i++) //BIDEA ETA KOSTEA PANTAILARATU
	{
		sprintf(str, "%d", bidea[i]);
		str[strlen(str)] = '\0';
		textuaGaitu();
        if (mapa_id==1)
        {
            textuaIdatzi(700, 142, "Bidea: ");
            textuaIdatzi(802 + 20 * i, 142, str);
        }
        else
        {
            textuaIdatzi(43, 142, "Bidea: ");
            textuaIdatzi(100 + 20 * i, 142, str);
        }
		sprintf(str, "%d", distantzia[amaieranodoa]);
		str[strlen(str)] = '\0';
		textuaGaitu();
        if (mapa_id==1)
        {
            textuaIdatzi(700, 262, "Kostea: ");
            textuaIdatzi(802, 262, str);
        }
        else
        {
            textuaIdatzi(43, 170, "Kostea: ");
            textuaIdatzi(105, 170, str);
        }
		pantailaBerriztu();
	}
}
NODO* irakurriNodoa(NODO* ptrelem, char fitxIzena[], MAPA* mapa, NODO* ptrlehena) //MAPAREN PUNTUAK KARGATZEKO FITXATEGITIK
{
    int zenbat = 0, i = 0,egoera=0;
    FILE* fitxategia = NULL;
    fitxategia = fopen(fitxIzena, "r");

    
        if (fitxategia > 0)
        {
            //ONDO ZABALDU DA
            printf("\n");
            egoera=fscanf(fitxategia, "%d\n", &mapa->id);
            do
            {
                ptrelem = (NODO*)malloc(sizeof(NODO));
                if (ptrelem != NULL)
                {
                    zenbat = fscanf(fitxategia, "%i %i\t%i %i\n", &ptrelem->x1, &ptrelem->x2, &ptrelem->y1, &ptrelem->y2);
                    if (zenbat > 0)
                    {
                        ptrelem->ptrHurrengoa = NULL;
                        ptrlehena = sartuBukaeran(ptrlehena, ptrelem);
                        i++;
                    }
                }


            } while (zenbat > 0);
            fclose(fitxategia);
        }
        else
        {
            printf("Ezin da zabaldu fitxategia.\n");
        }
        mapa->nodokop = i;
    
    return ptrlehena;
}

NODO* sartuBukaeran(NODO* lehena, NODO* berria) //PUNTUAK ZERRENDA BATEAN JASO
{
    NODO* aux = NULL;
    aux = lehena;
    if (lehena == NULL)
    {
        lehena = berria;
        berria->ptrHurrengoa = NULL;
    }
    else
    {
        while (aux->ptrHurrengoa != NULL)
        {
            aux = aux->ptrHurrengoa;
        }
        aux->ptrHurrengoa = berria;

    }
    return lehena;
}

