#ifndef MENUA_H
#define MENUA_H

typedef struct Nodo
{
    int x1;
    int x2;
    int y1;
    int y2;
    struct Nodo* ptrHurrengoa;
}NODO;

typedef struct Mapa
{
    NODO* ptrNodoak;
    int nodokop;
    int id;
}MAPA;
MAPA* sortumapa(char izena []);
POSIZIOA atzera(int* elem);
POSIZIOA mapa_aukeratu(POSIZIOA pos, int ebentu, MAPA* mapak, int* mapa);
POSIZIOA pasahitza_eskatu(POSIZIOA pos, int* ebentu, int* ondo);
char* izenak(POSIZIOA pos);
int nodoakAukeratu(MAPA* mapak, int nodokop);
void PantailaKargatu(char izena[]);
void sarreraMenua();
void PantailaKargatuKoord(char izena[], int x, int y);
void banderaKargatu(int x, int y);
int txakurraKargatu(int x, int y);
#endif 